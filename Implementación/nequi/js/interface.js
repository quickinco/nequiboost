Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };


var currentNot = 0;
$(document).ready(function(){
    
    init();
    
    var counters2 = 0;
    $("#s5").width($("#s5").width()+20);
    //S1
    $("#s1 button").click(function(e){
        e.preventDefault();
        change_screen(2);
    });
    //S2
    $("#s2 button").click(function(e){
        e.preventDefault();

        if(counters2==3){
            change_screen(3);
        }else
        {
            counters2++;
            $(".c"+counters2).addClass("filled");
        }
    });
    
    //s3
     $("#s3 button").click(function(e){
        e.preventDefault();
         change_screen(4);
        
    });
    
    //s4
    $("#s4 .add").click(function(e){
    
        e.preventDefault();
        change_screen(5);
    });
    $("#s4 .back").click(function(e){
    
        e.preventDefault();
        change_screen(3);
    });
    $("#s6 .back").click(function(e){
    
        e.preventDefault();
        change_screen(3);
    });
    //$("input.money").val("$0").data("vv",0);
    
    $("input.money").keyup(function(e){
        
        //$(this).val($(this).val()+""+String.fromCharCode(e.which)).addClass("used");
        //var prevVal = $(this).data("vv");
        //$(this).data("vv",String.fromCharCode(e.which));
        //console.log(parseInt($(this).data("vv")));
        //var vall = parseInt($(this).data("vv"));
        //console.log(vall);
        
    });
    $("#s5 aside span,#s5 aside ul li").click(function(){
        $("#s5 aside ul").toggleClass("opened");
    });
    $(document).on("click","#yei_spot .current .add",function(e){
        e.preventDefault();
        $(this).parent().removeClass("current");
        $(this).parent().parent().append('<li class="current"><input type="text" placeholder="Ej: Ver a mi banda favorita" /><a href="" class="add"></a></li>');
    });
    
    $("#s5 .back").click(function(e){
        e.preventDefault();
        change_screen(4);
    });
     $("#s5 .ok").click(function(e){
        e.preventDefault();
        get_alert(1,"Creaste tu primera meta","+5 puntos","feliz.gif",function(){
            $("#alert1").removeClass("opened");
            change_screen(6);
            //set_single_goal("Estereo Picnic con Pili",80000,1600000);
        });
    });
    $("#s6 .ranking").click(function(e){
        e.preventDefault();
        change_screen(7);
    });
    $("#s6 .gnrlbtn").click(function(e){
        e.preventDefault();
        change_screen(8);
    });
    $("#s7 a").click(function(e){
        e.preventDefault();
        change_screen(6);
    });
    
    $("#s8 .back").click(function(e){
        e.preventDefault();
        change_screen(6);
    });
    $("#s6 .link").click(function(e){
        e.preventDefault();
        get_alert(2,"","","",function(){
            console.log(1);
            
        });
    });
     $("#s9 button").click(function(e){
    
        e.preventDefault();
        change_screen(1);
    });
    $("footer").click(function(e){
        e.preventDefault();
        $("footer").animate({top:'-50px'},500,function(){
            if(currentNot==1)
            {
                change_screen(4);
            }
            if(currentNot==2)
            {
                change_screen(6);
            }
    });});
    
    $(".steps li a").click(function(e){
        e.preventDefault();
    });
    
    $("#s5 .modes li").click(function(e){
        e.preventDefault();
        $("#s5 .modes li").removeClass("active");
        $(this).addClass("active");
    });
    $("#s8 ul li span").click(function(e){
        e.preventDefault();
        change_screen(10);
    });
    $("#s10").click(function(){
        change_screen(11);
    });
    $("#s11 button").click(function(){
        get_alert(1,"¡Haz cumplido un reto de la semana!","+5 puntos","feliz.gif",function(){
            $("#alert1").removeClass("opened");
            change_screen(6);
            //set_single_goal("Estereo Picnic con Pili",80000,1600000);
        });
        
    });
    
});


function close_alert()
{
    $("article").removeClass("opened");
}
function change_screen(n)
{
    console.log(n);
    //buildScrolls();
    $("section").removeClass("active");
    $(".preloader").addClass("active");
    setTimeout(function() {
        $(".preloader").removeClass("active");
       $("#s"+n).addClass("active");
    }, 1000);
    
    if(n==6)
    {
        $("#s6 aside b").width(0);
        $("#s6 aside b").animate({width:$("#s6 aside strong").data("perc")+"%"},2500);
    }
    
}
function get_alert(n,title,subtitle,img,myfunction)
{
    $("article").removeClass("opened");

    $("#alert"+n).addClass("opened");
    if(n==1){
        $("#alert"+n+" h1").html(title);
        $("#alert"+n+" h2").html(subtitle);
        $("#alert"+n+" img").attr("src","images/"+img);
       
       }
    if(n==2)
    {
        $("#alert2 .gnrlbtn").click(function(e){
                e.preventDefault();
               get_alert(1,"¡Nuestros planes continúan!","","feliz.gif",function(){
                $("#alert1").removeClass("opened");

                });
                 
            });
        $("#alert2 .link").click(function(e){
            e.preventDefault();
             get_alert(1,"El Estereo Picnic con Pili salió de nuestros planes","","rota.gif",function(){
                $("#alert1").removeClass("opened");
                 change_screen(3);

            });
        });
        console.log("Alert 2");
    }
    if(n==3)
    {
        $("#alert3 .gnrlbtn").click(function(e){
                e.preventDefault();
                close_alert();
               change_screen(4);
                 
            });
        $("#alert3 .link").click(function(e){
            e.preventDefault();
             close_alert();
             change_screen(3);
        });
        
    }
   
    
    $("#alert"+n).click(myfunction);

}
function buildScrolls()
{
     if($(".scroll_element").length>0)
	{
		$(".scroll_element").mCustomScrollbar({
					set_width:false, /*optional element width: boolean, pixels, percentage*/
					set_height:true, /*optional element height: boolean, pixels, percentage*/
					horizontalScroll:false, /*scroll horizontally: boolean*/
					scrollInertia:550, /*scrolling inertia: integer (milliseconds)*/
					scrollEasing:"easeOutCirc", /*scrolling easing: string*/
					mouseWheel:"auto", /*mousewheel support and velocity: boolean, "auto", integer*/
					autoDraggerLength:true, /*auto-adjust scrollbar dragger length: boolean*/
					scrollButtons:{ /*scroll buttons*/
					enable:true, /*scroll buttons support: boolean*/
					scrollType:"continuous", /*scroll buttons scrolling type: "continuous", "pixels"*/
					scrollSpeed:20, /*scroll buttons continuous scrolling speed: integer*/
					scrollAmount:40 /*scroll buttons pixels scroll amount: integer (pixels)*/
					},
					advanced:{
					updateOnBrowserResize:true, /*update scrollbars on browser resize (for layouts based on percentages): boolean*/
					updateOnContentResize:true, /*auto-update scrollbars on content resize (for dynamic content): boolean*/
					autoExpandHorizontalScroll:false /*auto expand width for horizontal scrolling: boolean*/
					},
					callbacks:{
					onScroll:function(){}, /*user custom callback function on scroll event*/
					onTotalScroll:function(){}, /*user custom callback function on bottom reached event*/
					onTotalScrollOffset:0 /*bottom reached offset: integer (pixels)*/
					}
					}); 
	} 
}
function notification(msg)
{
    $("footer").html(msg).animate({top:0},500,function(){});
        
}
function invitation()
{
    currentNot = 1;
    notification("Aprovecha esta quincena para empezar una meta de ahorro y ganar premios");
}
function goal()
{
    change_screen(6);
}
function win()
{
    get_alert(3,"Creaste tu primera meta","+5 puntos","feliz.gif",function(){
            $("#alert1").removeClass("opened");
            change_screen(6);
            set_single_goal("Estereo Picnic con Pili",80000,1600000);
        });
}
function init()
{
    change_screen(9);
}
function motivation()
{
    currentNot = 2;
    notification("¿Sigues con planes de ver a los Foofighters? Continúa con tu meta");
}

